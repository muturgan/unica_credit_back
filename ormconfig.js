module.exports = {
    type: 'postgres',
    host: process.env.NODE_ENV === 'production' ? 'postgres' : 'localhost',
    port: process.env.APP_DATABASE_PORT || 5432,
    username: process.env.APP_DATABASE_LOGIN || 'root',
    password: process.env.APP_DATABASE_PASS || '360mysql',
    database: 'unica_credit',
    synchronize: true,
    logging: false,
    cli: {
      entitiesDir: `dist/db/entities`,
      migrationsDir: `dist/db/migrations`,
    },
    entities: [`${__dirname}/dist/db/entities/*.entity.js`],
    migrations: [`${__dirname}/dist/db/migrations/*.js`],
};
  