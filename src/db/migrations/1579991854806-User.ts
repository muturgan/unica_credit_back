import {MigrationInterface, QueryRunner} from 'typeorm';

export class User1579991854806 implements MigrationInterface {
   name = 'User1579991854806'

   public async up(queryRunner: QueryRunner): Promise<void> {
      await queryRunner.query(`
         CREATE TABLE "users" (
            "id" SERIAL NOT NULL,
            "email" character varying(32) NOT NULL,
            "first_name" character varying(32) NOT NULL,
            "last_name" character varying(32) NOT NULL,
            "avatar" text NOT NULL,
            CONSTRAINT "PK_a3ffb1c0c8416b9fc6f907b7433" PRIMARY KEY ("id")
         )
      `);
   }

   public async down(queryRunner: QueryRunner): Promise<void> {
      await queryRunner.query(`DROP TABLE "users"`);
   }

}
