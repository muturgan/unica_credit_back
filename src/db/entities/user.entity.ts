import { Entity, PrimaryGeneratedColumn, Column, BaseEntity } from 'typeorm';
// import { ApiProperty } from '@nestjs/swagger';
import { IUser } from '../../typings';

@Entity('users')
export class UserEntity extends BaseEntity
{
   // @ApiProperty({ example: 1, description: `user's id` })
   @PrimaryGeneratedColumn()
   public id: number;

   // @ApiProperty({ example: 'john.doe@gmail.cpm', description: `user's email` })
   @Column('varchar', { length: 32 })
   public email: string;

   // @ApiProperty({ example: 'John', description: `user's first name` })
   @Column('varchar', { length: 32 })
   public first_name: string;

   // @ApiProperty({ example: 'Doe', description: `user's last name` })
   @Column('varchar', { length: 32 })
   public last_name: string;

   // @ApiProperty({ example: 'https://some.image.url.jpg', description: `user's avatar url` })
   @Column('text')
   public avatar: string;



   public static getUsersCount(): Promise<number>
   {
      return UserEntity.count();
   }

   public static insertNewUsers(users: IUser[])
   {
      return Promise.all(
         users.map(u => UserEntity.insert(u)),
      );
   }

   public static findManyUsers(per_page: number = 0, page: number = 1): Promise<UserEntity[]>
   {
      return UserEntity.find({
         take: per_page,
         skip: page === 0 ? 0 : per_page * (page - 1),
      });
   }

   public static findUser(first_name?: string, last_name?: string): Promise<UserEntity | undefined>
   {
      if (first_name === undefined && last_name === undefined) {
         throw new Error('you should pass first_name and/or last_name');
      }

      const whereParams: {first_name?: string, last_name?: string} = {};

      if (typeof first_name === 'string' && first_name !== '') {
         whereParams.first_name = first_name;
      }

      if (typeof last_name === 'string' && last_name !== '') {
         whereParams.last_name = last_name;
      }

      return UserEntity.findOne({where: whereParams});
   }

}
