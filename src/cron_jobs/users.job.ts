import { CronJob } from 'cron';
import axios from 'axios';
import { IUser, IApiResponse } from '../typings';
import { UserEntity } from '../db';

const cronMinute = process.env.MUNUTE || 1;

const cronTime = ` */${cronMinute} * * * *`; // At every minute bi default

const getApiUrl = (count: number): string =>
{
   return count === 0
      ? `https://reqres.in/api/users`
      : `https://reqres.in/api/users?page=2&per_page=${count}`;
}

const fetchUsers = async (): Promise<void> =>
{
   try {
      const count = await UserEntity.getUsersCount();
      const url = getApiUrl(count);

      await axios.get<IApiResponse<IUser>>(url)
         .then(res => UserEntity.insertNewUsers(res.data.data));

   } catch (err) {
      console.error('Error on user fetching');
      console.error(err);
   }
};

export const usersJob = () =>
{
   fetchUsers();

   return new CronJob(
        cronTime,
        fetchUsers,
        undefined,
        true,
        'Europe/Moscow',
   );
};
