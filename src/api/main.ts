import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { FastifyAdapter, NestFastifyApplication } from '@nestjs/platform-fastify';
import { ApplicationModule } from './api.module';
import { usersJob } from '../cron_jobs';

async function bootstrap()
{
   const app = await NestFactory.create<NestFastifyApplication>(
      ApplicationModule,
		new FastifyAdapter(),
   );

   const options = new DocumentBuilder()
      .setTitle('Unica Credit test task backend documentation')
      .setDescription('Unica Credit test task API documentation')
      .setVersion('1.0.0')
      .addTag('swagger')
      .build();

   const document = SwaggerModule.createDocument(app, options);
   SwaggerModule.setup('swagger', app, document);

   await app.listen(3333, '0.0.0.0')
      .then(() => console.info('listening...'));

   usersJob();
}

bootstrap();
