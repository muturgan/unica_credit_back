import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
// import { Connection } from 'typeorm';
import { UsersModule } from './users/users.module';

@Module({
   imports: [
      UsersModule,
      TypeOrmModule.forRoot(),
	],
})
export class ApplicationModule
{
   // constructor(private readonly _conn: Connection)
   // {
   //    this._conn.runMigrations()
   //       .then(m => console.info(`${m.length} migrations were performed`))
   //       .catch(err => console.error(err));
   // }
}
