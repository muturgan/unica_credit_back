import { ApiProperty } from '@nestjs/swagger';
import { IUser } from '../../typings';

export class User implements IUser
{
   @ApiProperty({ example: 1, description: `user's id` })
   public id: number;

   @ApiProperty({ example: 'john.doe@gmail.cpm', description: `user's email` })
   public email: string;

   @ApiProperty({ example: 'John', description: `user's first name` })
   public first_name: string;

   @ApiProperty({ example: 'Doe', description: `user's last name` })
   public last_name: string;

   @ApiProperty({ example: 'https://some.image.url.jpg', description: `user's avatar url` })
   public avatar: string;
}
