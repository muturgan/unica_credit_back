import { IsString, IsOptional } from 'class-validator';

export class UserQueryDto
{
   @IsOptional()
   @IsString()
   readonly first_name?: string;

   @IsOptional()
   @IsString()
   readonly last_name?: string;
}
