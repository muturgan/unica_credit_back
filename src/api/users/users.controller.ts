import { Get, Controller, Query, NotFoundException, HttpException } from '@nestjs/common';
import { UserEntity } from '../../db';
// import { UserQueryDto } from './dto';


@Controller('api')
export class UsersController
{

   @Get('users')
   public getUsers(@Query('per_page') per_page?: string, @Query('page') page?: string): Promise<UserEntity[]>
   {
      const perPageNum = Number(per_page);
      const pageNum = Number(page);
      if (
         (
            typeof per_page === 'string'
            && Number.isNaN(perPageNum)
         ) || (
            typeof page === 'string'
            && Number.isNaN(pageNum)
         )
      ) {
         throw new HttpException('value of per_page or page query params should be an integer', 422);
      }

      if (Boolean(per_page) === false && Boolean(page) === true) {
         throw new HttpException('you can specify the page value only with specified per_page value', 422);
      }

      return UserEntity.findManyUsers(perPageNum, pageNum);
   }

   @Get('users/find')
   public async SearchRequest(@Query('first_name') first_name?: string, @Query('last_name') last_name?: string): Promise<UserEntity>
   {
      if ((first_name === undefined && last_name === undefined) || (first_name === '' && last_name === '')) {
         throw new NotFoundException();
      }

      const user = await UserEntity.findUser(first_name, last_name);

      if (user === undefined) {
         throw new NotFoundException();
      }
      return user;
   }

}
