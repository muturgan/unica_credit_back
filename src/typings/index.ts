export interface IApiResponse<T> {
   page: number;
   per_page: number;
   total: number;
   total_pages: number;
   data: T[];
}

export interface IUser {
   id: number;
   email: string;
   first_name: string;
   last_name: string;
   avatar: string;
}
